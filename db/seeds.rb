# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

places = ['東京', '大阪', '福岡', '札幌', '仙台', '名古屋', '金沢']
30.times do |n|
  Room.create!(name: "room\##{format("%02d", n)}", place: places[n % 7], number: 5)
end

20.times do |n|
  Entry.create!(user_name: "user#{n}", user_email: "user#{n}@example.com", reserved_date: Time.now + n.days - 10.days)
end

Rails.application.routes.draw do
  resources :users
  root to: 'top#index'
  get 'top/index'
  resources :rooms
  resources :entries, only: [:new, :create, :destroy, :index], path: :rentals do
    post :confirm, on: :collection
    get :confirm_back, on: :collection
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

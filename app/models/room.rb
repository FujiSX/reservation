class Room < ApplicationRecord
  has_many :entries, dependent: :destroy
  # validations
  validates :name, :place, :number, presence: true
  validates :name, length: { maximum: 30 }
  validates :place, inclusion: { in: ['東京', '大阪', '福岡', '札幌', '仙台', '名古屋', '金沢'] }
  validates :number, numericality: { less_than_or_equal_to: 5, more_than_or_equal_to: 30, }
  validates :name, format: { with: /\A\w+#\d{2}\z/, message: '会議室名が正しくありません' }
  validate :validate_number_of_people

  # callbacks
  before_validation :format_room_name

  private
  def validate_number_of_people
    unless self.number % 5 == 0
      self.errors[:number] << '収容人数は5の倍数で指定してください'
    end
  end

  def format_room_name
    trim_spaces_around_room_name if /\A\s+.+\s+\z/.match(self.name)
    remove_spaces_into_underscore_in_room_name if /(.+\s*)+/.match(self.name)
  end

  def trim_spaces_around_room_name
    self.name.strip!
  end

  def remove_spaces_into_underscore_in_room_name
    self.name.gsub!(/\s/, '_')
  end
end

class Entry < ApplicationRecord
  belongs_to :room
  scope :least_entries, ->(standard_date) { where(reserved_date: standard_date.to_date - 3.days .. standard_date.to_date + 3.days) }
end

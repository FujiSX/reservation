class ApplicationController < ActionController::Base
  ADMIN = { 'admin_user' => 'admin_password' }
  # before_action :authenticate

  private
  def authenticate
    authenticate_or_request_with_http_digest do |admin_username|
      ADMIN[admin_username]
    end
  end
end

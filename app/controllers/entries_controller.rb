class EntriesController < ApplicationController
  before_action :set_entry, only: [:destroy]
  def new
    @entry = Room.find(params[:room_id]).entries.new
  end

  def create
    @entry = Entry.new(entry_params)

    respond_to do |format|
      if @entry.save
        format.html { redirect_to @entry.room, notice: "予約が完了しました。"}
        format.json { render :index, status: :created, location: @entry }
      else
        format.html { render :new }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @entry.destroy
    respond_to do |format|
      # format.html { redirect_to room_path(@entry.room_id), notice: "予約を取り消しました。" }
      # format.json { head :no_content }
      format.js { head :no_content }
    end
  end

  def index
    @entries = Entry.all
  end

  def confirm
    @entry = Entry.new(entry_params)
    
    respond_to do |format|
      if @entry.valid?
        format.html { render 'confirm' }
        format.json { render json: :confirm }
      else
        format.html { redirect_to confirm_back_entries_path(entry: entry_params) }
      end
    end
  end

  def confirm_back
    @entry = Entry.new(entry_params)
    respond_to do |format|
      format.html { render :new, action: :new }
      format.json { render json: @book.errors, status: :unprocessable_entity }
    end
  end

  private
  def set_entry
    @entry = Entry.find(params[:id])
  end

  def entry_params
    params.require(:entry).permit(:user_name, :user_email, :reserved_date, :usage_time, :room_id, :people)
  end
end
